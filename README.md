## use git-crypt to encrypt the sensible files in git repository ##
required packages:

- gpg
- git-crypt

### prepare GPG ###
#### No GPG key ####
generate a GPG key `gpg --gen-key`

### prepare the git repository ###
1. configure a repository to use git-crypt `git-crypt init`
2. add one public key to encrypt the repository `git-crypt add-gpg-user --trusted your.email@domain.com`
3. specify files to encrypt by creating a [.gitattributes](.gitattributes) file
4. view the encrypt status `git-crypt status`
5. normal git flow, eg. `add` `commit` `push`
6. after cloning a repository with encrypted files, unlock with GPG: `git-crypt unlock`

### reference ###

- [git-crypt - transparent file encryption in git](https://www.agwa.name/projects/git-crypt/)
